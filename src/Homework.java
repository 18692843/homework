import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.File;

import java.io.FileWriter;
import java.io.BufferedWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Homework {
    private JPanel root;
    private JLabel topLabel;
    private JButton RisottoButton;

    private JButton TofuButton;
    private JButton CrepeButton;
    private JButton Fried_riceButton;
    private JButton Beef_bowlButton;
    private JButton Squid_bowlButton;
    private JTextArea textArea1;
    private JButton checkOutButton;
    private JLabel Total;
    private JButton cancelButton;

    String orderedItems = "";
    int totalPrice;

    void message(String food, int price) {
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like " + food + " ?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);
        if (confirmation == 0) {
            totalPrice += price;
            orderedItems = orderedItems + food + "\t" + price + " yen\n";
            textArea1.setText(orderedItems);
            Total.setText("Total: " + String.valueOf(totalPrice) + " yen");
            JOptionPane.showMessageDialog(null, "Order for " + food + " received");
        }
    }

    void Reset() {
        orderedItems = "";
        totalPrice = 0;
        textArea1.setText(orderedItems);
        Total.setText("Total: " + String.valueOf(totalPrice) + " yen");
    }

    public Homework() {
        RisottoButton.setIcon(new ImageIcon(
                this.getClass().getResource("risotto.jpg")));

        TofuButton.setIcon(new ImageIcon(
                this.getClass().getResource("tofu.jpg")));

        CrepeButton.setIcon(new ImageIcon(
                this.getClass().getResource("crepe.jpg")));

        Fried_riceButton.setIcon(new ImageIcon(
                this.getClass().getResource("fried rice.jpg")));

        Beef_bowlButton.setIcon(new ImageIcon(
                this.getClass().getResource("beef bowl.jpg")));

        Squid_bowlButton.setIcon(new ImageIcon(
                this.getClass().getResource("squid Bowl.jpg")));

        RisottoButton.addActionListener(new ActionListener() {
            @Override

            public void actionPerformed(ActionEvent e) {
                message("Risotto", 1500);


            }
        });
        TofuButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                message("Tofu", 1450);
            }
        });
        CrepeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                message("Crepe", 1990);
            }
        });
        Fried_riceButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                message("Fried rice", 700);
            }
        });
        Beef_bowlButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                message("Beef bowl", 390);
            }
        });
        Squid_bowlButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                message("Squid bowl", 1000);
            }
        });
        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                int confirmation = JOptionPane.showConfirmDialog(null,
                        "Would you like to checkout ?",
                        "Checkout Confirmation",
                        JOptionPane.YES_NO_OPTION);
                if (confirmation == 0) {
                    JOptionPane.showMessageDialog(null, "Thank you, Total price is "
                            + totalPrice + "yen");

                    LocalDateTime date1 = LocalDateTime.now(); //現在時間の取得
                    DateTimeFormatter dtFormat = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm"); //現在時間の表示フォーマット
                    String fDate1 = dtFormat.format(date1); //表示形式+Stringに変換

                    // 出力する文字列
                    String order = "---------\t\tRECEIPT\t\t--------\n\n"
                                + "\tRestaurant zouzei\t\n"
                                + "\tDate: " + fDate1 + "\t\n\n"
                                + orderedItems +
                                "\n\tTotal Price:\t" + totalPrice + " yen"  + "\n\n---------\tTY FOR ORDER!\t--------";
                    String fileName = "Receipt0.txt";
                    File file = new File(fileName); //ファイル作成

                    int i = 0;

                    try {
                        while(true) {   //レシート生成で前のレシートを上書きさせない
                            if (file.exists()) {
                                i++;
                                fileName = "Receipt" + i + ".txt";
                                file = new File(fileName);
                            } else {
                                // ファイルを開いて書き込み用のBufferedWriterを作成
                                fileName = "Receipt" + i + ".txt";
                                BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));//ある程度データを貯めてから一度に出力
                                // 文字列をファイルに書き込む
                                writer.write(order);

                                // ファイルを閉じる
                                writer.close();

                                break;
                            }
                        }

                    } catch (IOException f) {
                        System.err.println("ファイルに書き込む際にエラーが発生しました: " + f.getMessage());
                    }
                    Reset();
                }
            }
        });


        cancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "Would you like to cancel ?",
                        "Cancel Confirmation",
                        JOptionPane.YES_NO_OPTION);
                if (confirmation == 0) {
                    Reset();
                    JOptionPane.showMessageDialog(null, "Cancellation was successful");



                }
            }
        });
    }

        public static void main (String[]args){
            JFrame frame = new JFrame("Homework");
            frame.setContentPane(new Homework().root);
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame.pack();
            frame.setVisible(true);
        }
    }